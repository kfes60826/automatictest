﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using System.Text;

namespace ProduceAutoTestCode
{
    public class ProduceCode
    {
        public IConfigurationRoot Builder()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory) 
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true); 

            return builder.Build();
        }

        public bool CreateAutoTestFile(string folderPath) {

            IConfigurationRoot builder = Builder();

            string sourceFolderPath = builder["Path:SourceFoldPath"]!;
            string targetFolderPath = builder["Path:TargetFoldPath"]!;

            //取得資料夾下所有.cs檔的路徑
            IEnumerable<string> filePaths = GetAllFilePath(sourceFolderPath);

            //處理每個.cs檔案
            foreach (string filePath in filePaths) {
                //取得.cs內的所有 function 名稱
                (string, IEnumerable<FileInfo>)  csFileInfo = GetAllFunName(filePath);

                //取得檔案內容(新增) 
                string fileName = Path.GetFileName(filePath).Replace(".cs", "Test.cs"); //檔案名稱
                string content = CreateCsFileContent(csFileInfo.Item1 + ".TEST", fileName.Replace(".cs",""), csFileInfo.Item2);

                CreateCsFile(targetFolderPath, fileName, content);
            }

            return true;
        }

        //取得.cs內的所有 function 名稱
        public (string,IEnumerable<FileInfo>) GetAllFunName(string filePath)
        {
            // 讀取檔案內容
            string code = File.ReadAllText(filePath);

            // 使用SyntaxTree解析程式碼
            SyntaxTree tree = CSharpSyntaxTree.ParseText(code);

            // 取得根節點
            CompilationUnitSyntax root = tree.GetCompilationUnitRoot();

            // 找出所有的方法，並取得名稱和路徑
            var methodNames = root.DescendantNodes()
                                      .OfType<MethodDeclarationSyntax>()
                                      //.Where(method => method.AttributeLists.Any(attrList => attrList.Attributes
                                      //    .Any(attr => attr.Name.ToString().Contains("Http"))))
                                      .Select(method => new FileInfo
                                      {
                                          name = method.Identifier.ValueText,
                                          routeAttribute = GetRouteFromHttpPostAttribute(method)
                                      });
            // 找出 namespace 宣告
            string namespaceName = "";
            NamespaceDeclarationSyntax namespaceDeclaration = root.DescendantNodes().OfType<NamespaceDeclarationSyntax>().FirstOrDefault();
            // 檢查是否有找到 namespace 宣告
            if (namespaceDeclaration != null)
            {
                namespaceName = namespaceDeclaration.Name.ToString();
            }

            return (namespaceName,methodNames);
        }

        // 從HttpPost屬性中提取路徑
        public RouteAttributeIInfo GetRouteFromHttpPostAttribute(MethodDeclarationSyntax method)
        {
            var routeAttribute = method.AttributeLists
                                      .SelectMany(attrList => attrList.Attributes)
                                      .FirstOrDefault(attr => attr.Name.ToString().Contains("Http"));

            var routeArgument = routeAttribute?.ArgumentList?.Arguments.FirstOrDefault()?.ToString();
            var routeName = routeAttribute?.Name?.ToString();
            return new RouteAttributeIInfo { route=routeArgument ?? "", attributeName = routeName ?? "" };
        }

        //取得資料夾下所有.cs檔的路徑和檔案名稱
        public IEnumerable<string> GetAllFilePath(string folderPath)
        {
            // 使用 LINQ 查詢取得所有 .cs 檔案的路徑
            var csFilePaths = Directory.EnumerateFiles(folderPath, "*.cs", SearchOption.AllDirectories);

            return csFilePaths;
        }

        //新增檔案
        public bool CreateCsFile(string folderPath, string fileName, string content)
        {
            // 確認目標資料夾存在
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            // 建立檔案路徑
            string filePath = Path.Combine(folderPath, fileName);

            // 檢查檔案是否已存在，如已存在則不再建立
            if (!File.Exists(filePath))
            {
                // 建立並寫入檔案內容
                File.WriteAllText(filePath, content);
                Console.WriteLine($"檔案已建立：{filePath}");
            }
            else
            {
                Console.WriteLine($"檔案已存在：{filePath}");
            }

            return true;
        }


        //取得檔案內容(新增)
        public string CreateCsFileContent(string nameSpace,string className, IEnumerable<FileInfo> functionInfos) {

            // 建立字串建構器
            StringBuilder codeBuilder = new StringBuilder();

            // 添加 using 指示詞
            codeBuilder.AppendLine("using NUnit.Framework;");
            codeBuilder.AppendLine("using ForAutomaticTest.Controllers;");
            codeBuilder.AppendLine("using System;");
            codeBuilder.AppendLine("using System.Collections.Generic;");
            codeBuilder.AppendLine("using System.Linq;");
            codeBuilder.AppendLine("using System.Text;");
            codeBuilder.AppendLine("using System.Threading.Tasks;");
            codeBuilder.AppendLine("using JsonSerializer = System.Text.Json.JsonSerializer;");
            codeBuilder.AppendLine("using Microsoft.AspNetCore.Mvc.Testing;");
            codeBuilder.AppendLine();

            // 添加 namespace 宣告
            codeBuilder.AppendLine(string.Format(@"namespace {0}", nameSpace));
            codeBuilder.AppendLine("{");

            // 添加類別 (class) 宣告
            codeBuilder.AppendLine(string.Format(@"    public class {0}", className));
            codeBuilder.AppendLine("    {");

            //若functionInfos有包含route，則宣告WebApplicationFactory和HttpClient(測試api用)
            if (functionInfos.Any(o => o.routeAttribute.route != "")) {
                codeBuilder.AppendLine("        private WebApplicationFactory<Program> _factory = null!;");
                codeBuilder.AppendLine("        private HttpClient _client = null!;");
                codeBuilder.AppendLine("");
                codeBuilder.AppendLine("        [OneTimeSetUp]");
                codeBuilder.AppendLine("        public void Init()");
                codeBuilder.AppendLine("        {");
                codeBuilder.AppendLine("            _factory = new WebApplicationFactory<Program>();");
                codeBuilder.AppendLine("            _client = _factory.CreateClient();");
                codeBuilder.AppendLine("        }");
            }

            // 添加函數 (function) 宣告
            foreach (var functionInfo in functionInfos) {
                //function名稱
                codeBuilder.AppendLine(string.Format(@"        public async Task {0}()", functionInfo.name));
                codeBuilder.AppendLine("        {");

                //function內容
                //若有route，就測試api路徑
                if (!string.IsNullOrEmpty(functionInfo.routeAttribute.route) && false) {
                    string routeData = "{ name = \"John\", password = \"pw\" }";
                    codeBuilder.AppendLine("            var data = new "+ routeData + ";");
                    if (functionInfo.routeAttribute.attributeName == "HttpPost") {
                        codeBuilder.AppendLine("            var content = new StringContent(JsonSerializer.Serialize(data), Encoding.UTF8, \"application/json\");");
                        codeBuilder.AppendLine("            var response = await _client." + functionInfo.routeAttribute.attributeName.Replace("Http", "") + "Async(" + functionInfo.routeAttribute.route + ", content);");
                    }
                }

                //若沒"Assert"，則寫入"Assert.Fail();"
                if (!codeBuilder.ToString().Contains("Assert"))
                {
                    codeBuilder.AppendLine("            Assert.Fail();");
                }

                //function結尾
                codeBuilder.AppendLine("        }");
            }

            // 關閉類別 (class) 宣告
            codeBuilder.AppendLine("    }");

            // 關閉 namespace 宣告
            codeBuilder.AppendLine("}");

            return codeBuilder.ToString();
        }
    }

    public class FileInfo()
    {

        public string name = string.Empty;

        public RouteAttributeIInfo routeAttribute = null;

        //public string routeAttribute = string.Empty;

        
    }

    public class RouteAttributeIInfo() {

        public string route = string.Empty;
        public string attributeName = string.Empty;

    }
}
